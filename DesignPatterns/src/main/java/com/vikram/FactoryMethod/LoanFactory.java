package com.vikram.FactoryMethod;

public class LoanFactory {
	
	public Loan getLoan(String loanType)
	{
		if(loanType == null)
		{
			return null;
		}
		else if(loanType.equalsIgnoreCase("Personal"))
		{
			return new Personal();
		}
		else if(loanType.equalsIgnoreCase("Car"))
		{
			return new Car();
		}
		else if(loanType.equalsIgnoreCase("Mortgage"))
		{
			return new Mortgage();
		}
		return null;
	}
}
