package com.vikram.FactoryMethod;

public abstract class Driver {

	protected String driver = "";
	abstract void getDriver();
	
	public String initDriver()
	{
		return driver;
	}
}

class Firefox extends Driver
{
	public void getDriver()
	{
		driver = "Firefox";
	}
}

class Internet  extends Driver
{
	public void getDriver()
	{
		driver = "Internet Explorer";
	}
}

class Chrome  extends Driver
{
	public void getDriver()
	{
		driver = "Chrome";
	}
}