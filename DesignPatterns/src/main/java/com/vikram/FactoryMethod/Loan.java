package com.vikram.FactoryMethod;

abstract class Loan {

	public double rate;
	
	abstract double getRate();
	
	public double calculateInterest(double principle, int term)
	{
		return principle*rate*term;
	}
}

class Personal extends Loan
{

	double getRate() {
		rate = 7.5;
		return rate;
	}
	
}

class Mortgage extends Loan
{
	double getRate()
	{
		rate = 3.90;
		return rate;
	}
}

class Car extends Loan
{
	double getRate()
	{
		rate = 6.5;
		return rate;
	}
}
