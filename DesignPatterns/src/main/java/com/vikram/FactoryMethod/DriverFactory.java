package com.vikram.FactoryMethod;

public class DriverFactory {
	
	public Driver getDriver(String driverType)
	{
		if(driverType.equalsIgnoreCase(null))
			return null;
		else if (driverType.equalsIgnoreCase("Firefox"))
		{
			return new Firefox();
		}
		else if (driverType.equalsIgnoreCase("chrome"))
		{
			return new Chrome();
		}
		else if (driverType.equalsIgnoreCase("ie"))
		{
			return new Internet();
		}
		else return null;
	}
}
