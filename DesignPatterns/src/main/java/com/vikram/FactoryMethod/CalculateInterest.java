package com.vikram.FactoryMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CalculateInterest {

	public static void main(String args[]) throws IOException
	{
		LoanFactory loanFactory = new LoanFactory();
		System.out.println("Enter the loan type:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
    
		String loanType = br.readLine();
		Loan loan = loanFactory.getLoan(loanType);
		
		System.out.println("Enter the principle amount:");
		String amount = br.readLine();
		
	
		System.out.println("The rate for " + loanType + " loan is:" + loan.getRate()	+ 
				" and Interest charged for 5 years " + loan.calculateInterest(Double.parseDouble(amount), 5));
		
	}

}
