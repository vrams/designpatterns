package com.vikram.FactoryMethod;

public class CreateDriver {

	public static void main(String args[])
	{
	DriverFactory driverFactory = new DriverFactory();
	
	Driver driver = driverFactory.getDriver("chrome");
	driver.getDriver();
	System.out.println(driver.initDriver());
	}

}
