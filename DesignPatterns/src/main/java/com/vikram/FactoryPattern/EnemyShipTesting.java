package com.vikram.FactoryPattern;
import java.util.Scanner;

public class EnemyShipTesting {
	
	public static void main(String args[])
	{
		EnemyShipFactory shipFactory = new EnemyShipFactory();
		EnemyShip theEnemy = null;
		Scanner userInput = new Scanner (System.in);
		System.out.println("What type of Ship? (U / R / B)");
		
		if(userInput.hasNextLine())
		{
			String typeOfShip = userInput.nextLine();
			theEnemy = shipFactory.makeEnemeyShip(typeOfShip);
		}
		if(theEnemy != null)
		{
			doStuffEnemy(theEnemy);
			
		}else 
		System.out.println("Please use the correct option next time");
	}

	public static void doStuffEnemy(EnemyShip anEnemyShip)
	{
		anEnemyShip.displayEnemyShip();
		anEnemyShip.followHeroShip();
		anEnemyShip.enemyShipShoots();
	}
}
