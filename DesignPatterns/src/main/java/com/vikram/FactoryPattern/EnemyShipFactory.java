package com.vikram.FactoryPattern;

public class EnemyShipFactory {
	
	public EnemyShip makeEnemeyShip(String newShipType)
	{
		EnemyShip newShip = null;
		if(newShipType.equals("U"))
		{
			return new UFOEnemyShip();
		} 
		else if(newShipType.equals("R"))
		{
			return new UFOEnemyShip();
		}
		else if(newShipType.equals("B"))
		{
			return new RocketEnemyShip();
		}
		else return null;
			
	}

}
