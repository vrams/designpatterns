package com.vikram.StrategyPattern;

public class StratergyDesign {
public static void main(String args[])
{
	Animal sparky = new Dog();
	Animal tweety = new Bird();
	System.out.println("Dog: " + sparky.tryToFly());
	System.out.println("Burd: " + tweety.tryToFly());
	sparky.setFlyingAbility(new FlyFast());
	System.out.println("Dog: " + sparky.tryToFly());
}
}
