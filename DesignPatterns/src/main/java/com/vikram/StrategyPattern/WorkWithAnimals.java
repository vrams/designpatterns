package com.vikram.StrategyPattern;

public class WorkWithAnimals {
	
	int num = 10;

	public static void main(String args[])
	{
		Dog fido = new Dog();
		fido.setName("Fido");
		System.out.println(fido.getName());
		fido.digHole();
		fido.setWeight(-1);
		int randNum = 10;
		fido.changeVar(randNum);
		System.out.println("randnum after method call " + randNum);
		changeObjectName(fido);
		System.out.println("After Method " + fido.getName());
		
		
		Animal doggy = new Dog();
		Animal kitty = new Cat();
		System.out.println("Doggy Says: "+ doggy.getSound());
		System.out.println("Kitty Says: "+ kitty.getSound());
		
		Animal[] animals = new Animal[4];
		animals[0] = doggy;
		animals[1] = kitty;
		
		System.out.println("Doggy Says: "+ animals[0].getSound());
		System.out.println("Kitty Says: "+ animals[1].getSound());
		
		speakAnimal(doggy);
		((Dog)doggy).digHole();
		
		Giraffe giraffe = new Giraffe();
		
		giraffe.setName("Frank");
		System.out.println(giraffe.getName());
		
	}
	
	public static void changeObjectName(Dog fido)
	{
		fido.setName("Marcus");
	}
	
	public static void speakAnimal(Animal randAnimal)
	{
		System.out.println("Animal Says: " + randAnimal.getSound());
	}
	
	public void sayHello()
	{
		System.out.println("Say hello");
	}
	
	
}
