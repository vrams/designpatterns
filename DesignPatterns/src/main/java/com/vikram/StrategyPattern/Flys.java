package com.vikram.StrategyPattern;

public interface Flys {

	String fly();
	
}

class ItFlys implements Flys
{

	public String fly() {
		return "Flying High";
	}
	
}

class CantFly implements Flys
{

	public String fly() {
		return "Can't Fly";
	}
	
}
class FlyFast implements Flys
{
	public String fly()
	{
		return "Flysing super fast";
	}
}