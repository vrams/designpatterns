package com.vikram.StrategyPattern;

public class Dog extends Animal
{
	public void digHole()
	{
		System.out.println("Dug a hole");
	}
	
	public Dog()
	{
		super();
		setSound("Bark");
		flyingType = new ItFlys();
	}
	
	public void changeVar(int randNum)
	{
		randNum = 12;
		System.out.println("Rand num in the method " + randNum);
	}
}
